<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<?php

// location of the text file that will log all the ip adresses
$file = 'logfile.txt';

// ip address of the visitor
$ipadress = $_SERVER['REMOTE_ADDR'];

// date of the visit that will be formated this way: 29/May/2011 12:20:03
$date = date('d/F/Y h:i:s');

// name of the page that was visited
$webpage = $_SERVER['SCRIPT_NAME'];

// visitor's browser information
$browser = $_SERVER['HTTP_USER_AGENT'];

// Opening the text file and writing the visitor's data
$fp = fopen($file, 'a');

fwrite($fp, $ipadress.' - ['.$date.'] '.$webpage.' '.$browser."\r\n");

fclose($fp);

?>

<head>

	<!--
		::::::::::
		::::  ::::
		::::  ::::
		::::  ::::
		:::::::::: SQUARE 1
	-->
	
	<meta charset="utf-8">
	<title>LENSAR | Cataract Laser with Augmented Reality</title>
	<meta name="description" content="Laser Refractive Cataract Surgery">
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Mobile Only  -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

	<!-- CSS -->
	<link rel="stylesheet" href="css/base.css">
	<link rel="stylesheet" href="css/skeleton.css">
	<link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="css/mediaelementplayer.css" />    
    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="css/ccslider.css" />
    
    <!--[if IE 7]>
    <link rel="stylesheet" href="stylesheets/ie7.css">
    <![endif]-->

	<!-- Web and Mobile Favicons  -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">    
    
    <!-- Scripts  -->
    <script src="js/jquery-1.7.min.js" type="text/javascript"></script>
	<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="js/jquery.animate-colors-min.js" type="text/javascript"></script>
	
    <!--/***********************************************
    * Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
    * This notice MUST stay intact for legal use
    * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
    ***********************************************/-->
    <script src="js/ddsmoothmenu.js" type="text/javascript"></script>
    <script src="js/jquery.cssAnimate.mini.js" type="text/javascript"></script>
    <script src="js/mediaelement-and-player.min.js"></script>
    <script src="js/screen.js" type="text/javascript"></script>
    
    <style type="text/css">		  	
    	#slider {
    		width: 1040px;
    		height: 400px;
    	}
    	
    	#slider .control-links {
    		margin-top: -15px;
    	}
    	
    	#demo-description {
    		max-width: 900px;
    		width: 90%;
    		margin: 60px auto;
    		color: #fff;
    		font: 20px 'PT Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    		text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5);
    	}
    </style>
    
    <!-- The google stuff  -->
    <script type="text/javascript">
    
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-35522137-1']);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    
    </script>  
</head>

<body>
	<div class="poswrapheaderline"><div class="headerline"></div></div>  
    <div class="tiledbackground"></div>
    <div class="poswrapper"><div class="whitebackground"></div></div>
	<div class="container main portfolio4column">
    
        <!-- Header -->	
		<div class="sixteen columns header">       
			<a href="index.html" class="logohover"><div class="logo"></div></a>
            <div class="mainmenu">
                <div id="mainmenu" class="ddsmoothmenu">
                    <ul>
                        <li>
                        	<a href="./">HOME<br/></a>                          
                        </li>
                        <li><a href="about.html">ABOUT US<br/><span>Info, News &amp Events</span></a>
                            <ul> 
                                 <li><a href="about.html">Our Mission</a>
                                 <ul>
                                     <li><a href="med_board.html">Medical Advisory Board</a></li>
                                     <li><a href="lensar_board.html">LENSAR Advisory Board</a></li>
                                     <li><a href="directors_board.html">Board of Directors</a></li>  
                                 </ul>
                                 </li> 
                                 <li><a href="news.html">Our News</a></li>
                                 <li><a href="events.html">Upcoming Events</a></li>
                                 <li><a href="careers.html">Careers</a></li>
                            </ul>
                        </li>
                       <li><a href="system.html">TECHNOLOGY<br/><span>Precision &amp Predictability</span></a>
                           <ul> 
                           	<li><a href="system.html">The LENSAR Laser System</a></li>
                           	<li><a href="ar.html">Augmented Reality</a>
                           		<ul>
                           		    <li><a href="tilt.html"> Intelligent Lens Tilt Detection </a></li>
                           		    <li><a href="planning.html"> Flexible Procedure Planning </a></li>		           
                           		</ul>
                           	</li>
                           	   <li><a href="incisions.html">Smart Corneal Incisions</a></li>
                               <li><a href="features.html">Full Features</a></li>
                               <li><a href="testimonials.html">Testimonials</a></li>
                               
                           </ul>
                       </li>
                        <li><a href="cataracts.html">PATIENTS<br/><span>The LENSAR Difference</span></a>
                            <ul>  
                                <li><a href="cataracts.html">What Are Cataracts</a></li>
                                <li><a href="lensar.html">What Is LENSAR</a></li>
                                <li><a href="how.html">How Does LENSAR Work</a></li>
                                <li><a href="difference.html">What Are the Benefits of LENSAR</a></li>
                            </ul>
                        </li>
                        	
                        <li><a href="contact.html">CONTACT<br/><span>Get In Touch</span></a></li>	
                    </ul>
                    <br style="clear: left" />
                </div>
                
                <!-- Responsive Menu -->               
				<form id="responsive-menu" action="#" method="post">
                    <select>
                        <option value="">Navigation</option>
                    </select>
				</form>
				
            </div>
		</div>		
        
        <!-- Title -->       
		<div class="pagetitle">
        	<!--<div class="pagetitleholder"><h1>Learn more about the new reality in cataract surgery. <a href="http://lensar2012.com">Meet us at AAO 2012.</a> </h1>
        	</div>-->
           <!-- <div class="socialholder">
            	
            </div>-->
        </div>
		
        <!-- Slider -->       
		<div class="homeslider row"> 
			<div id="caption6" class="cc-caption"><a href="testimonials.html">Efficient lens fragmentation reduces phaco energy up to 100%.<sup>1</sup><span>Learn More ></span></a></div>	 
			<div id="caption4" class="cc-caption"><a href="planning.html">Automated features and thoughtful ergonomics allow for seamless integration without added procedure time.<span>Learn More ></span></a></div>
			<div id="caption5" class="cc-caption"><a href="ar.html">Precise 3-D reconstruction of the relevant anterior segment gives you the confidence to safely treat all cataract densities.<span>Learn More ></span></a></div>
				     
          	<div id="slider">
          		
          		   
          		<img src="images/banner/banner6.jpg" alt="" data-href="planning.html" data-captionelem="#caption4" data-caption-position="bottom" data-transition='{"pauseTime": 6000,"effect": "cubeRight", "slices": 5, "delay": 400}'/> 	
          		<img src="images/banner/banner4.jpg" alt="" data-href="ar.html" data-captionelem="#caption5" data-caption-position="bottom" data-transition='{"pauseTime": 6000,"effect": "cubeRight", "slices": 5, "delay": 400}'/>
          		<img src="images/banner/banner5.jpg" alt="" data-href="testimonials.html" data-captionelem="#caption6" data-caption-position="bottom" data-transition='{"pauseTime": 6000,"effect": "cubeUp", "slices": 9, "animSpeed": 1200, "delay": 400, "delayDir": "fromCentre", "easing": "easeInOutBack", "depthOffset": 300, "sliceGap": 30}'/>
          						
          	</div>          	
          	
          	
          	<!-- CCSlider --> 
          	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>              	
          	<script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
          	<script src="js/jquery.tools.tooltip.min.js"></script>	
          	<script src="js/jquery.easing.1.3.js"></script>
          	<script src="js/jquery.ccslider-3.0.1.min.js"></script>	         	
          	<script>
          		$(window).load(function(){
          			$('#slider').ccslider({
          				_3dOptions: {
          					imageWidth: 1040,
          					imageHeight: 400
          				},
          				directionNav: true,
          				controlLinks: true,
          				controlLinkThumbs: false,
          				controlThumbLocation: '',
          				pauseTime: 7000
          			});
          		});        		
          	</script>       	
		</div>
		<div id="aao2013_tout">
			<h2>AAO 2013 – <span>BOOTH 2154</span></h2>Come experience the intelligent approach to laser cataract surgery at LENSAR BOOTH 2154.<br/><a class="aaobutton" href="./AAO2013/">Learn More About LENSAR at AAO 2013</a>
		</div>	
        <!-- Tout -->       
		<div class="sixteen columns row textblock">
		
            <div class="one_third">
                <h5 class="teaserheadline"><img src="images/lensarIcon.jpg" alt="" />Superior Technology</h5>
                <p>The LENSAR Laser System was thoughtfully designed with refractive cataract surgery in mind. <br/><br/><a href="system.html" class="button">Learn More</a></p>
            </div>
            <div class="one_third">
                <h5 class="teaserheadline"><img src="images/lensarIcon.jpg" alt="" />Smarter Patient Choice</h5>
                <p>Learn about the LENSAR Laser System and how it can improve your cataract surgery outcome. <br/><br/><a href="cataracts.html" class="button">Learn More</a></p>
            </div>
            <div class="one_third lastcolumn">
                <h5 class="teaserheadline"><img src="images/lensarIcon.jpg" alt="" />LENSAR In Action</h5>
                <p>View the latest surgical footage and surgeon testimonials for the LENSAR Laser System. <br/><br/><a href="news.html" class="button">Learn More</a></p>
            </div>       
            <div class="clear"></div>
        </div>
        
		<div class="sixteen columns bottomadjust"><h8>1. Data on file. LENSAR, Inc.</h8></div>           
	</div>

    <!--  footer -->
	<div class="container footerwrap">   
    	<div class="footerclose"></div>   
        <div class="footer">
        	<div class="sixteen columns">             
                <div class="twelve columns clearfix widget alpha">
                   <h5>LATEST TWEETS</h5>
                    <div class="widget_tweets">
                    	<script>
                    		String.prototype.linkify = function() {
                    			return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/, function(m) {
                    					return m.link(m);
                    			})/*.split('<a href').join('</br><a href').split('/a>').join('/a></br>')*/;
                    		};
                    		
                    		function relative_time(time_value) {
                    		  var values = time_value.split(" ");
                    		  time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
                    		  var parsed_date = Date.parse(time_value);
                    		  var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
                    		  var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
                    		  delta = delta + (relative_to.getTimezoneOffset() * 60);
                    		
                    		  var r = '';
                    		  if (delta < 60) {
                    		        r = 'a minute ago';
                    		  } else if(delta < 120) {
                    		        r = 'couple of minutes ago';
                    		  } else if(delta < (45*60)) {
                    		        r = (parseInt(delta / 60)).toString() + ' minutes ago';
                    		  } else if(delta < (90*60)) {
                    		        r = 'an hour ago';
                    		  } else if(delta < (24*60*60)) {
                    		        r = '' + (parseInt(delta / 3600)).toString() + ' hours ago';
                    		  } else if(delta < (48*60*60)) {
                    		        r = '1 day ago';
                    		  } else {
                    		        r = (parseInt(delta / 86400)).toString() + ' days ago';
                    		  }
                    		  
                    		  return r;
                    		}
                    		jQuery.ajaxSetup({ cache: true });
                    		jQuery.getJSON("http://api.twitter.com/1/statuses/user_timeline.json?callback=?&screen_name=lensar&count=3", function(data){
                    			
                    			jQuery.each(data, function(index, item){
                    					
                    					var $twlink = item.text.linkify();
                    					
                    					jQuery(".widget_tweets ul").append("<li>" + $twlink + "<div class='subline'>" + relative_time(item.created_at) + "</div></li>");
                    					
                    			});
                    			jQuery(".footer .widget_tweets a").hover(function() {
                    				jQuery(this).animate({ color: $apex_highlightcolor },{duration:200,queue:false}, 'easeOutSine');
                    			},function() {
                    				jQuery(this).animate({ color: "#ccc" },{duration:300,queue:false}, 'easeOutSine');
                    			});
                    			jQuery(".sidebar .widget_tweets a").hover(function() {
                    				jQuery(this).animate({ color: $apex_highlightcolor },{duration:200,queue:false}, 'easeOutSine');
                    			},function() {
                    				jQuery(this).animate({ color: "#333" },{duration:300,queue:false}, 'easeOutSine');
                    			});
                    		});
                    	</script>
                        <ul></ul>
                        <div class="clear"></div>
                    </div>
                </div>                
                <div class="three columns widget ">
                    <h5>Connect with us</h5>
                    <div class="widget_social">
                        <ul>                     	
                           <a href="https://www.facebook.com/pages/LensAR/111968775492501" class="borderhover"><img src="images/soc/facebook.png" alt="" /></a>
                           <a href="http://twitter.com/LensAR" class="borderhover"><img src="images/soc/twitter.png" alt="" /></a>
                           <a href="http://www.linkedin.com/company/1023420" class="borderhover"><img src="images/soc/linkedin.png" alt="" /></a>
                           <a href="http://www.youtube.com/user/LensARinc" class="borderhover"><img src="images/soc/youtube.png" alt="" /></a>                         
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>            
            </div>
        </div>
	</div>
    
    <!-- Legal and Indications -->
    <div class="container subfooterwrap">   
    	<div class="footeropen"></div>  
    	<div class="subfooter">
        	<div class="ten columns siteinfo"></div>
            <div class="six columns sitenav">
                <a href="./">Home</a>&nbsp; &nbsp; &nbsp;            
                <a href="terms.html">Terms of Use</a>&nbsp; &nbsp; &nbsp;
                <a href="contact.html">Contact</a>
            </div>
        </div>
        <div id="legal">
        	<p class="style4"> 
        	    <strong>Indication</strong>
        	     <p>The LENSAR Laser System – fs 3D (LLS-fs 3D) is intended for use in patients undergoing cataract surgery for removal of the crystalline lens.  Intended uses in cataract surgery include anterior capsulotomy, laser phacofragmentation, and the creation of full and partial thickness single-plane and multi-plane arc cuts/incisions in the cornea, each of which may be performed either individually or consecutively during the same procedure.</p>
        	     <p>Laser Capsulotomy, laser phacofragmentation and/or corneal incisions surgery is contraindicated in patients: who are of pediatric age, whose pupils will not dilate or remain dilated to a diameter greater than that of the intended treatment and for capsulotomies and/or laser phacofragmentation with intended diameters of less than 4 mm or greater than 7 mm, who have existing corneal implants, who have previous corneal incisions that might provide a potential space into which the gas produced by the procedure can escape, who have conditions that would cause inadequate clearance between the intended capsulotomy cut and the corneal endothelium, such as: hypotony, uncontrolled glaucoma, who have corneal disease or pathology that precludes transmission of light at the laser wavelength or causes distortion of laser light, such as: corneal opacities, residual, recurrent, active ocular or uncontrolled eyelid disease or any corneal abnormalities (including endothelial dystrophy, guttata, recurrent corneal erosion, etc.) in the eye to be treated, ophthalmoscopic signs of keratoconus (or keratoconus suspect) in the eye to be treated, a history of severe dry eye that has not responded to therapy, a history of herpes zoster or herpes simplex keratitis.</p>
        	     <p>Potential contraindications are not limited to those included in the list.</p>
        	     <p>WARNING: The safety and effectiveness of this laser have NOT been established in patients with diabetic retinopathy, a history of treated glaucoma, or prior intraocular surgery.</p>
        	     <p>© 2013 LENSAR, Inc.  All rights reserved.  LENSAR, the LENSAR logo and Augmented Reality are trademarks of LENSAR, Inc.</p>
        	    <p>50-00004-000 Rev. F 09/13</p> 
        	</p>         
        </div>
    </div>
</body>
</html>